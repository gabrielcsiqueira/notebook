class Contact < ApplicationRecord
  belongs_to :kind # contato pertence a um tipo
  has_one :address# contato possui um endereço
  has_many :phones, dependent: :destroy# contato possui muitos telefones

  accepts_nested_attributes_for :address #aceita atributos de endereço
  accepts_nested_attributes_for :phones, reject_if: :all_blank, allow_destroy: true
end
